import json
from datetime import datetime
from io import BytesIO
from stat import S_ISREG,S_ISDIR
import boto3
import paramiko
from botocore.exceptions import ClientError
from decouple import config
import pandas as pd
import numpy


def lambda_handler(event, context):
    print("Event:", event)
    print("Context:", context)
    return {
        'status': 200,
        'message': "It's Working...!!"
    }
